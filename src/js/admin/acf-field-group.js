(function($){


acf.field_group.field_object = acf.field_group.field_object.extend({
	actions: {
		'render_settings'	: 'frontend_render'
	},
	events: {
		'change [data-frontend-setting="enable"] [type="checkbox"]' : 'frontend_render',
		'change [data-frontend-setting="wrapper"] select' 			: 'frontend_render',
		'dragstart [data-insert-placeholder]' 						: 'frontend_start_drag_placeholder',
	},
	frontend_render: function( $el ) {

		// show/hide other frontend settings

		if ( this.setting('frontend input[type="checkbox"]').prop('checked') ) {

			this.$settings.find('[data-frontend-setting="enable"] ~ [data-frontend-setting]').show();

		} else {

			this.$settings.find('[data-frontend-setting="enable"] ~ [data-frontend-setting]').hide();

		}


		// enable / disable attribute input

		this.setting('frontend-wrapper [type="text"]').prop( 'disabled', this.setting('frontend-wrapper select').val() === '' )

	},
	// drag placeholder
	frontend_start_drag_placeholder: function(e) {
		e.originalEvent.dataTransfer.setData( 'text/plain', $(e.target).data('insert-placeholder') );
	}

});


var acf_settings_frontend_wrapper = acf.field_group.field_object.extend({

	type: 'frontend_wrapper',

	actions: {
		'render_settings': 'render'
	},

	events: {
//			'change .acf-field-setting-ui input': 'render'
	},

	render: function( $el ){
		console.log($el);
	}

});


})(jQuery)

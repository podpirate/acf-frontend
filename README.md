ACF Frontend
============

Latin Capital Letter S With Combining Burdurian Mustache Accent
0x0053 + 0x1DFA / 0x1ABF


ToDo
----
 - [ ] Output-locations
	 - [x] Post
	 - [x] Widget
	 - [ ] User
	 - [ ] Taxonomy
 - [ ] Frontend Output
	- [ ] Uncategorized
		- [ ] Group
	- [ ] Single Scalar
		- [x] Button group
		- [x] Date Picker
		- [x] Page Link
		- [ ] <del>Message</del>
		- [x] Number
		- [x] Text
		- [x] Range
		- [x] Time Picker
		- [ ] True False
			- Shall I ..?
		- [x] Wysiwyg
		- [x] Radio
		- [x] Email
		- [x] Textarea
		- [x] URL
		- [x] Select
		- [ ] Color Picker
			- Option: is Background color of Parent Structure `<SELECT:FieldGroup,...>`
	- [ ] Sequence
		- [x] Flexible Content
		- [x] Checkbox
		- [x] Repeater
		- [ ] Relationship
			- Link option
		- [ ] Taxonomy
			- Link option
			- Option: render as link | render ACF fieldset contents
	- [ ] Relational
		- [ ] Link
		- [x] Post Object (optional multiple)
		- [x] Relationship (always multiple)
		- [ ] Taxonomy (optional multiple)
		- [ ] User (optional multiple)
	- [ ] Media
		- [ ] File
		- [ ] Image
			- Link option?
		- [ ] Gallery
			- Option: render as link | render WP Gallery
	– [ ] Embedding
		- [ ] Google Map
		- [ ] OEmbed

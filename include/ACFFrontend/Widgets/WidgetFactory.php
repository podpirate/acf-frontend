<?php

namespace ACFFrontend\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}
use ACFFrontend\Core;

class WidgetFactory extends Core\Singleton {
	// parent of checkbox

	/**
	 * Sets up a new Archives widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct() {
		$this->renderer = Core\Renderer::instance();
		add_action('widgets_init', array( $this, 'widgets_init') );
		add_filter('widget_display_callback',array( $this, 'widget_display_callback' ), 10, 3);
	}

	/**
	 *	@action init
	 */
	public function widgets_init( ) {
		foreach ( array( 'Foo', 'Bar', 'Quux' ) as $name ) {
			$inst = new BaseWidget( $name );
			register_widget( $inst );
		}
	}

	/**
	 *	@filter widget_display_callback
	 */
	public function widget_display_callback( $instance, $wp_widget, $args ) {

		$return = $instance;
		$content = '';

		$field_groups = acf_get_field_groups( array( 'widget' => $wp_widget->id_base ) );
		$field_groups_to_render = array();
		foreach ( $field_groups as $field_group ) {
			if ( ! isset( $field_group['frontend_active'] ) || ! $field_group['frontend_active'] ) {
				continue;
			}
			switch ( $field_group['frontend_location']['widget'] ) {
				case 'widget':
					$return = false;
					$field_groups_to_render = array( $field_group );
					break;
				case 'widget_before':
					$field_groups_to_render[] = $field_group;
					break;
			}
		}

		if ( empty( $field_groups_to_render ) ) {
			return $return;
		}

		foreach ( $field_groups_to_render as $field_group ) {
			$rendered = $this->renderer->render_field_group( $field_group, 'widget_'.$wp_widget->id );
			switch ( $field_group['frontend_location']['widget'] ) {
				case 'widget':
					$content = $rendered;
					break;
				case 'widget_before':
					$content = $rendered . $content;
					break;
			}
		}
		echo $content;

		return $return;
	}
}

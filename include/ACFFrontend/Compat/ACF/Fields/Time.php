<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) )
	die('Nope.');

class Time extends Generic {

	/**
	 *	@inheritdoc
	 */
	public function render_output( $post_id, $placeholders = array() ) {

		$output = '';

		if ( isset( $this->acf_field['frontend_wrapper'] ) && ( $value = $this->get_value( $post_id ) ) ) {
			// add datetime wrapper attribute
			if ( isset( $this->acf_field['frontend_wrapper'] ) && 'time' === $this->acf_field['frontend_wrapper']['type'] ) {

				$unformatted = get_field( $this->acf_field['key'], $post_id, false );

				$this->acf_field['frontend_wrapper']['attr'] = $this->acf_field['frontend_wrapper']['attr'] + array(
					'datetime'	=> strftime( '%Y-%m-%d %H:%M:%S', strtotime( $unformatted ) ),
				);

			}

		}

		$output = parent::render_output( $post_id, $placeholders );

		return $output;

	}

}

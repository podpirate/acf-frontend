<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

class GenericAttribute extends Generic {

	/**
	 *	@inheritdoc
	 */
	public function __construct( $acf_field ) {

		parent::__construct( $acf_field );

		if ( isset($this->acf_field['frontend_attr']) && $this->acf_field['frontend_attr']['attr'] ) {
			add_filter( "acf_frontend_wrapper_attributes_{$this->acf_field['frontend_attr']['parent']}",array( $this, 'add_attr' ), 10, 2 );
		}
	}

	/**
	 *	@filter acf_frontend_wrapper_attributes_{$field_id}
	 */
	public function add_attr( $attr, $post_id ) {
		$attr[ $this->acf_field['frontend_attr']['attr'] ] = $this->get_value( $post_id );
		return $attr;
	}

}

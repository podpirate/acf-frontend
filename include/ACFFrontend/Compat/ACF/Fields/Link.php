<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) )
	die('Nope.');

class Link extends Generic {

	/**
	 *	@inheritdoc
	 */
	public function get_value( $post_id, $format_value = true ) {

		if ( $value = parent::get_value( $post_id, $format_value ) ) {
			if ( $this->acf_field['frontend_link'] ) {
				$value = $this->renderer->wrap( $value, array(
					'type'	=> 'a',
					'attr'	=> array(
						'href'	=> $value,
					),
				));
			}
		}

		return $value;
	}

}

<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

class RepeaterField extends Sequence {

	private $current_post_id = null;

	public function render_output( $post_id, $placeholders = array() ) {
		$output = '';


		$this->current_key = $this->acf_field['key'];
		$inner_tag = $outer_tag = '';
		if ( have_rows( $this->current_key, $post_id ) ) {

			add_filter('acf_frontend_field_value',array( $this, 'get_sub_value' ),10,2);

			$outer_tag = $inner_tag = $head = '';

			switch ( $this->acf_field['frontend_wrapper']['type'] ) {
				case 'table':
//					$outer_tag = 'tr';
					$outer_tag = 'tbody';
					$inner_tag = 'tr';
					$head = ''; // table heading!
					foreach ( $this->acf_field['sub_fields'] as $sub_field ) {
						if ( ! $sub_field['frontend'] ) {
							continue;
						}
						$head .= $this->renderer->wrap( $sub_field['label'], array('type'=>'th') );
					}
					$head = $this->renderer->wrap( $head, array('type'=>'tr') );
					$head = $this->renderer->wrap( $head, array('type'=>'thead') );
					break;
				case 'ul':
				case 'ol':
					$inner_tag = 'li';
					break;
			}
			$i = 0;
			while ( have_rows( $this->current_key, $post_id ) ) {
				the_row();

				// render sub fields
				$this->current_post_id = $post_id;

				$output .= $this->renderer->render_sequence(
					array( 'frontend_wrapper' => array(
						'type' => $inner_tag,
					) ),
					$this->acf_field['sub_fields'],
					$post_id,
					array(
						'id'		=> $this->acf_field['ID'],
						'name'		=> $this->acf_field['name'],
						'key'		=> $this->acf_field['key'],
						'i'			=> ++$i,
					)

				);
				$i++;
//				$output .= $this->renderer->wrap($row,array('type'=>'tr'));
			}

			$output = $head . $this->renderer->wrap( $output, array('type' => $outer_tag ) );
			$output = $this->renderer->wrap( $output, $this->acf_field['frontend_wrapper'], array(
				'id'	=> $this->acf_field['id'],
				'name'	=> $this->acf_field['name'],
				'key'	=> $this->acf_field['key'],
			) );

			remove_filter('acf_frontend_field_value',array($this,'get_sub_value'),10);
		}
		return $output;
	}

	public function get_sub_value( $value, $field, $format_value = true ) {
		return get_sub_field( $field->acf_field['key'], $this->current_post_id, $format_value );
	}
}

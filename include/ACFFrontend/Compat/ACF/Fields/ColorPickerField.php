<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

class ColorPickerField extends GenericAttribute {

	/**
	 *	@filter acf_frontend_wrapper_attributes_{$field_id}
	 */
	public function add_attr( $attr, $post_id ) {
		$attr = wp_parse_args($attr, array(
			'style' => '',
		));
		if ( $color = $this->get_value( $post_id ) ) {
			$attr[ 'style' ] .= sprintf('%s:%s;', $this->acf_field['frontend_attr']['attr'], $color );			
		}
		return $attr;
	}

}

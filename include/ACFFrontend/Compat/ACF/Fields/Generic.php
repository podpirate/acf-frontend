<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) )
	die('Nope.');

class Generic extends Field {

	/**
	 *	@inheritdoc
	 */
	public function get_value( $post_id, $format_value = true ) {

		if ( $value = apply_filters( 'acf_frontend_field_value', false, $this ) ) {
			return $value;
		}

		$value = get_field( $this->acf_field['key'], $post_id, $format_value );

		return apply_filters( 'acf_frontend_field_value', $value, $this );
	}


	/**
	 *	@inheritdoc
	 */
	public function render_output( $post_id, $placeholders = array() ) {

		$output = '';

		if ( isset( $this->acf_field['frontend_wrapper'] ) && ( $value = $this->get_value( $post_id ) ) ) {

			$output = $this->renderer->wrap( $value, $this->acf_field['frontend_wrapper'], $placeholders + array(
				'id'	=> $this->acf_field['ID'],
				'name'	=> $this->acf_field['name'],
				'key'	=> $this->acf_field['key'],
				'value'	=> $value,
			) );
		}

		return $output;
	}

}

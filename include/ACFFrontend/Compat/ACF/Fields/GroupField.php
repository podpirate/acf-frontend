<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

class GroupField extends Sequence {

	private $current_value = array();

	public function render_output( $post_id, $placeholders = array() ) {
		// should be Field:::get_value() !
		$this->current_value = $this->get_value( $post_id );

		add_filter( 'acf_frontend_field_value', array( $this, 'get_sub_value' ), 10, 2 );
		$output = $this->renderer->render_sequence( $this->acf_field, $this->acf_field['sub_fields'], $post_id, $placeholders );
		remove_filter( 'acf_frontend_field_value', array( $this, 'get_sub_value' ), 10 );

		return $output;
	}
	/**
	 *	@param string $value
	 *	@param Field $field
	 */
	public function get_sub_value( $value, $field ) {

		if ( isset( $this->current_value[ $field->acf_field['name'] ] ) ) {
			return $this->current_value[ $field->acf_field['name'] ];
		}
		return '';
	}
}

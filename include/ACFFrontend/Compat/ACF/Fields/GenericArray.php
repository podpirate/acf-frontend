<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

class GenericArray extends GenericAttribute {

	/**
	 *	@inheritdoc
	 */
	public function is_supported() {
		return true;
	}

	public function get_value( $post_id, $format_value = true ) {
		$value = parent::get_value( $post_id, $format_value );

		if ( is_array( $value ) ) {
			$value = implode( ', ', $value );
		}

		return $value;
	}

}

<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use ACFFrontend\Core;

abstract class Field {

	protected static $fields;

	protected $acf_field = array();

	protected $renderer = null;


	/**
	 *	Factory method
	 *	@param string $field_type
	 *	@return class derived class of ACFFrontend\Compat\ACF\Fields\Field
	 */
	public static function getFieldClass( $field_type ) {
		$cl = get_called_class();
		$ns = substr( $cl, 0, strrpos( $cl, '\\'));

		$field_class = explode( '_', $field_type );
		$field_class = array_map( 'ucfirst', $field_class );
		$field_class = $ns . '\\' . implode( '', $field_class ) . 'Field';

		if ( ! class_exists( $field_class ) ) {
			$field_class = $ns . '\\Generic';
		}
		return $field_class;
	}

	/**
	 *	Factory method
	 *	@param array $acf_field
	 *	@return ACFFrontend\Compat\ACF\Fields\Field
	 */
	public static function getFieldObject( $acf_field ) {
		if ( ! $acf_field || is_null( $acf_field ) ) {
			return;
		}

		if ( ! isset( self::$fields[ $acf_field['key'] ] ) ) {
			// complete field
			$acf_field = wp_parse_args( $acf_field, array(
				// defaults
			));
			$field_class = self::getFieldClass( $acf_field['type'] );

			self::$fields[ $acf_field['key'] ] = new $field_class( $acf_field );
		}

		return self::$fields[ $acf_field['key'] ];
	}

	public function __construct( $acf_field ) {
		$this->renderer = Core\Renderer::instance();
		$this->acf_field = $acf_field;
	}

	/**
	 *	@return string
	 */
	abstract function get_value( $post_id, $format_value = true );

	/**
	 *	@return string
	 */
	abstract function render_output( $post_id, $placeholders = array() );



}

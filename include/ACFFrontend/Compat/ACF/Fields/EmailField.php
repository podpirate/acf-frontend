<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) )
	die('Nope.');

class EmailField extends Link {

	/**
	 *	@inheritdoc
	 */
	public function get_value( $post_id, $format_value = true ) {

		if ( $value = Generic::get_value( $post_id, $format_value ) ) {
			if ( $this->acf_field['frontend_link'] ) {
				$value = $this->renderer->wrap( $value, array(
					'type'	=> 'a',
					'attr'	=> array(
						'href'	=> 'mailto:' . $value,
					),
				));
			}
		}

		return $value;
	}

}

<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) )
	die('Nope.');

class RangeField extends GenericAttribute {

	/**
	 *	@inheritdoc
	 */
	public function render_output( $post_id, $placeholders = array() ) {

		$output = '';
		if ( isset( $this->acf_field['frontend_wrapper'] ) && 'progress' === $this->acf_field['frontend_wrapper']['type'] ) {
			$this->acf_field['frontend_wrapper']['attr'] = $this->acf_field['frontend_wrapper']['attr'] + array(
				'min'	=> $this->acf_field['min'] ? $this->acf_field['min'] : '0',
				'max'	=> $this->acf_field['max'] ? $this->acf_field['max'] : '100',
				'value'	=> $this->get_value( $post_id )
			);
		}
		$output = parent::render_output( $post_id, $placeholders );

		return $output;

	}

}

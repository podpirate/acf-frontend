<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

class Sequence extends Generic {
	// parent of checkbox

	public function render_output( $post_id, $placeholders = array() ) {
		return $this->renderer->render_sequence( $this->acf_field, $this->get_value( $post_id ), $post_id, $placeholders );
	}

}

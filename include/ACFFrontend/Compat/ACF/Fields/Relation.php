<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Core;

class Relation extends Sequence {
	// parent of checkbox

	public function render_output( $post_id, $placeholders = array() ) {
		$output = '';
		if ( $this->acf_field['frontend_content'] === 'link' ) {

		} else if ( $this->acf_field['frontend_content'] === 'acf' ) {
			/*

			*/
			$value = (array) get_field( $this->acf_field['key'], $post_id, false );
			// naked post id
			foreach ( $value as $post_id ) {
				$output .= Core\Content::instance()->render( '', intval( $post_id ) );
			}

		}
		return $output;
	}

}

<?php

namespace ACFFrontend\Compat\ACF\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

class FlexibleContentField extends Sequence {

	private $current_post_id = null;

	public function render_output( $post_id, $placeholders = array() ) {
		$output = '';

		$this->current_key = $this->acf_field['key'];
		$inner_tag = $outer_tag = '';
		if ( have_rows( $this->current_key, $post_id ) ) {

			add_filter('acf_frontend_field_value',array( $this, 'get_sub_value' ),10,2);

			$i = 0;
			while ( have_rows( $this->current_key, $post_id ) ) {
				the_row();
				$layout = get_row_layout();
				// render sub fields
				$this->current_post_id = $post_id;
				$layout_object = $this->get_layout_object( $layout );

				$output .= $this->renderer->render_sequence(
					$this->acf_field,
					$layout_object['sub_fields'],
					$post_id,
					array(
						'id'		=> $this->acf_field['ID'],
						'name'		=> $this->acf_field['name'],
						'key'		=> $this->acf_field['key'],
						'layout'	=> $layout,
						'i'			=> ++$i,
					)
				);
			}

			// $output = $this->renderer->wrap( $output, $this->acf_field['frontend_wrapper'], array(
			// 	'name'		=> $this->acf_field['name'],
			// 	'key'		=> $this->acf_field['key'],
			// 	'layout'	=> $layout,
			// ) );

			remove_filter('acf_frontend_field_value',array($this,'get_sub_value'),10);
		}
		return $output;
	}
	/**
	 *	@inheritdoc
	 */
	public function get_sub_value( $value, $field, $format_value = true ) {
		return get_sub_field( $field->acf_field['key'], $this->current_post_id, $format_value );
	}

	private function get_layout_object( $layout = null ) {
		if ( is_null( $layout ) ) {
			$layout = get_row_layout();
		}
		foreach ( $this->acf_field['layouts'] as $layout_object ) {
			if ( $layout === $layout_object['name'] ) {
				return $layout_object;
			}
		}
	}

}

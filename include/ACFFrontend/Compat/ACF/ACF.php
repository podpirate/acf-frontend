<?php

namespace ACFFrontend\Compat\ACF;

use ACFFrontend\Core;


/**
 *	Maintain compatibility with polylang
 */
class ACF extends Core\Singleton {

	protected function __construct() {
		if ( is_admin() ) {
			Admin\FieldGroup::instance();
		}
	}

}

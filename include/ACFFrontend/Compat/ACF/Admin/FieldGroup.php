<?php

namespace ACFFrontend\Compat\ACF\Admin;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use ACFFrontend\Core;
use ACFFrontend\Compat\ACF\Fields as FrontendFields;
use ACFFrontend\Compat\ACF\Admin\Fields as AdminFields;

class FieldGroup extends Core\Singleton {
	/**
	 *	@inheritdoc
	 */
	protected function __construct() {
		// add output metabox
		add_action('acf/input/admin_head', array( $this, 'add_meta_box' ) );
		add_action('acf/input/admin_head', array( $this, 'sort_meta_boxes' ), 100 );

		add_action('acf/render_field/type=frontend_wrapper', array( $this, 'render_wrapper_setting' ) );
		add_action('acf/render_field/type=frontend_placeholder', array( $this, 'render_placeholder_setting' ) );
		add_action('acf/render_field/type=frontend_attribute', array( $this, 'render_attribute_setting' ) );

		add_action( 'acf/render_field_settings' , array( $this, 'render_acf_settings' ) );

		add_action('acf/field_group/admin_enqueue_scripts', array($this,'enqueue_assets'), 11 );
	}

	public function enqueue_assets() {
		$core = Core\Core::instance();
		wp_enqueue_script( 'acf-frontend-field-group', $core->get_asset_url( 'js/admin/acf-field-group.js' ) );
		wp_enqueue_style( 'acf-frontend-field-group', $core->get_asset_url( 'css/admin/acf-field-group.css' ) );
	}

	/**
	 *	@action acf/render_field_settings
	 */
	public function render_acf_settings( $acf_field ) {

		$field = AdminFields\Field::getFieldObject( $acf_field );

		if ( ! $field->is_supported() ) {
			return;
		}

		$this->render_frontend_toggle( $acf_field );

		$this->render_frontend_wrapper( $acf_field );

		$field->render_acf_settings( $acf_field );
	}

	/**
	 *	Frontend on/off Setting
	 *
	 *	@param array $acf_field
	 */
	public function render_frontend_toggle( $acf_field ) {
		$acf_field = wp_parse_args( $acf_field, array(
			'frontend' => false,
		));
		acf_render_field_setting( $acf_field, array(
			'label'			=> __('Enable Frontend Output','acf-frontend'),
			'instructions'	=> '',
			'type'			=> 'true_false',
			'name'			=> 'frontend',
			'class'			=> 'acf-frontend-toggle',
			'ui'			=> true,
			'wrapper'		=> array(
				'data-frontend-setting'	=> 'enable',
			)
		));
	}

	/**
	 *	Wrapper Setting
	 *
	 *	@param array $acf_field
	 */
	public function render_frontend_wrapper( $acf_field ) {

		$field = AdminFields\Field::getFieldObject( $acf_field );

		if ( $wrapper_types = $field->get_wrapper_types() ) {
			if ( $acf_field['ID'] ) {
				$class = 'acf-field-setting-frontend-wrapper';
				$instructions = __('You can use use Placeholders in the wrapper attributes:', 'acf-frontend' );
				// $instructions .= ' ' . __('<code>{name}</code>, <code>{value}</code>, <code>{key}</code> and <code>{value:&lt;field_name&gt;}</code>.', 'acf-frontend' );
				// $instructions .= ' ' . __('<code>&lt;field_name&gt;</code> will be replaced with the value of a subfield.','acf-frontend');
			} else {
				$class = 'acf-field-group-setting-frontend-wrapper';
				$instructions = '';
			}

			$acf_field = wp_parse_args( $acf_field, array(
				'frontend_wrapper' => array(),
				'frontend_placeholders'	=> array(),
			));



			acf_render_field_setting( $acf_field, array(
				'label'					=> __('Wrapper','acf-frontend'),
				'instructions'			=> $instructions,
				'type'					=> 'frontend_wrapper',
				'prefix'				=> 'acf_field_group',
				'frontend_wrapper'		=> $acf_field['frontend_wrapper'],
				'ui'					=> 0,
				'wrapper_type_choices'	=> $wrapper_types,
				'frontend_placeholders'	=> $field->get_attribute_placeholders(),
				'wrapper'	=> array(
					'class'	=> $class,
					'data-frontend-setting'	=> 'wrapper',
				),
			));

		}
	}

	/**
	 *	Wrapper Setting fields
	 *
	 *	@param array $acf_field
	 *	@action acf/render_field/type=frontend_wrapper
	 */
	public function render_wrapper_setting( $acf_field ) {

		$acf_field['frontend_wrapper'] = wp_parse_args( $acf_field['frontend_wrapper'], array(
			'type'		=> '',
			'attr'		=> array(),
		));
		$acf_field['frontend_wrapper']['attr'] = wp_parse_args( $acf_field['frontend_wrapper']['attr'], array(
			'class'		=> '',
			'id'		=> '',
		));

		acf_render_field_wrap( array(
			'label'			=> '',
			'instructions'	=> '',
			'type'			=> 'select',
			'name'			=> 'type',
			'prefix'		=> $acf_field['prefix'] .'[frontend_wrapper]',
			'value'			=> $acf_field['frontend_wrapper']['type'],
			'ui'			=> 0,
			'prepend'		=> __('Type', 'acf-frontend'),
			'choices'		=> $acf_field['wrapper_type_choices'],
			'wrapper'		=> array(
				'data-frontend-setting'	=> 'wrapper',
			),
		),'div','label');

		// wrapper
		acf_render_field_wrap(array(
			'label'			=> '',
			'instructions'	=> '',
			'type'			=> 'text',
			'name'			=> 'class',
			'prefix'		=> $acf_field['prefix'] .'[frontend_wrapper][attr]',
			'value'			=> $acf_field['frontend_wrapper']['attr']['class'],
			'prepend'		=> __('class', 'acf-frontend'),
		),'div','label');

		acf_render_field_wrap(array(
			'label'			=> '',
			'instructions'	=> '',
			'type'			=> 'text',
			'name'			=> 'id',
			'prefix'		=> $acf_field['prefix'] .'[frontend_wrapper][attr]',
			'value'			=> $acf_field['frontend_wrapper']['attr']['id'],
			'prepend'		=> __('id', 'acf-frontend'),
		),'div','label');

		if ( ! empty( $acf_field['frontend_placeholders'] ) ) {
			?>
			<p class="description placeholders">
				<?php
					_e( 'Drag Placeholder:', 'acf-frontend' );

					foreach ( $acf_field['frontend_placeholders'] as $code => $label ) {
						printf( '<code rel="button" draggable="true" data-insert-placeholder="{%s}">%s</code>', $code, $label );
					}

				?>
			</p>
			<?php

		}


	}

	/**
	 *	Link on/off setting
	 *
	 *	@param array $field
	 */
	public function render_link_setting( $field ) {
		$field = wp_parse_args( $field, array(
			'frontend_link' => true,
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Output Link','acf-frontend'),
			'instructions'	=> '',
			'type'			=> 'true_false',
			'name'			=> 'frontend_link',
			'class'			=> 'acf-frontend-link',
			'ui'			=> true,
			'wrapper'		=> array(
				'data-frontend-setting'	=> 'link',
				'class'	=> 'acf-field-setting-frontend-link',
			),
		));
		// link label: page title, select scalar sibling field, ...
	}

	/**
	 *	Select what to render (Relationship)
	 *
	 *	@param array $field
	 */
	public function render_content_setting( $field ) {
		$field = wp_parse_args( $field, array(
			'frontend_content' => 'link',
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Render','acf-frontend'),
			'instructions'	=> '',
			'type'			=> 'radio',
			'name'			=> 'frontend_content',
			'class'			=> 'acf-frontend-content',
			'ui'			=> false,
			'default'		=> 'link',
			'choices'		=> array(
				'link'	=> __('Link','acf-frontend'),
				'acf'	=> __('ACF-Content','acf-frontend'),
			),
			'wrapper'		=> array(
				'data-frontend-setting'	=> 'content',
				'class'	=> 'acf-field-setting-frontend-link',
			),
		));
	}

	/**
	 *	Wrapper Setting
	 *
	 *	@param array $acf_field
	 */
	public function render_frontend_attribute( $acf_field ) {
		$acf_field = wp_parse_args( $acf_field, array(
			'frontend_attr' => '',
			'frontend_attr_choices'	=> array(
				''					=> __('None','acf-frontend'),
				// 'color'				=> __('Text Color','acf-frontend'),
				// 'background-color'	=> __('Background Color','acf-frontend'),
				'title'				=> __('Title','acf-frontend'),

			),
			'frontend_attr_other_choice'	=> true,
		));

		acf_render_field_setting( $acf_field, array(
			'label'					=> __('Parent Field Property','acf-frontend'),
			'instructions'			=> '',
			'type'					=> 'frontend_attribute',
			'prefix'				=> 'acf_field_group',
			'frontend_attr'			=> $acf_field['frontend_attr'],
			'ui'					=> 0,
			'field'					=> $acf_field,
			'frontend_attr_choices'	=> $acf_field['frontend_attr_choices'],
			'frontend_attr_other_choice'	=> $acf_field['frontend_attr_other_choice'],
			'wrapper'	=> array(
				'data-frontend-setting'	=> 'attr',
			),
		));
	}

	/**
	 *	Wrapper Setting fields
	 *
	 *	@param array $acf_field
	 *	@action acf/render_field/type=frontend_wrapper
	 */
	public function render_attribute_setting( $acf_field ) {

		$acf_field['frontend_attr'] = wp_parse_args( $acf_field['frontend_attr'], array(
			'attr'		=> '',
			'parent'	=> 0,
		));
		$parents = array();

		$parent_id = $acf_field['field']['ID'];
		while ( $parent_id = wp_get_post_parent_id( $parent_id ) ) {

			$parent = get_post($parent_id);
			$pto = get_post_type_object( $parent->post_type );
			$parents[$parent_id] = sprintf('%s: %s', $pto->labels->singular_name, $parent->post_title );
		}
		//

		acf_render_field_wrap( array(
			'label'			=> __('Property','acf-frontend'),
			'instructions'	=> '',
			'type'			=> 'radio',
			'name'			=> 'attr',
			'prefix'		=> $acf_field['prefix'] .'[frontend_attr]',
			'value'			=> $acf_field['frontend_attr']['attr'],
			'ui'			=> 0,
			'prepend'		=> __('Type', 'acf-frontend'),
			'choices'		=> $acf_field['frontend_attr_choices'],
			'other_choice'	=> $acf_field['frontend_attr_other_choice'],
			'wrapper'		=> array(
				'data-frontend-setting'	=> 'attr',
			),
		),'div','label');

		// wrapper
		acf_render_field_wrap(array(
			'label'			=> __('Parent Field','acf-frontend'),
			'instructions'	=> '',
			'type'			=> 'radio',
			'name'			=> 'parent',
			'prefix'		=> $acf_field['prefix'] .'[frontend_attr]',
			'value'			=> $acf_field['frontend_attr']['parent'],
			'choices'		=> $parents,
		),'div','label');

	}



	public function get_wrapper_types( ) {
		$sections = func_get_args();
		$types = array(
			''	=> __( 'No Wrapper', 'acf-frontend' ),
		);

		foreach ( $sections as $section ) {
			$key = false;
			$add_types = array();
			switch ( $section ) {
				case 'sequence':
					$key = __( 'Sequential', 'acf-frontend' );
					$add_types = array(
						'table'		=> __( 'Table Rows', 'acf-frontend' ),
						'nav'		=> __( 'Navigation', 'acf-frontend' ),
						'ul'		=> __( 'Unordered List', 'acf-frontend' ),
						'ol'		=> __( 'Ordered List', 'acf-frontend' ),
						'dl'		=> __( 'Definition List', 'acf-frontend' ),
					);
					break;
				case 'block':
					$key = __( 'Block Level', 'acf-frontend' );
					$add_types = array(
						'div'			=> __( 'Div', 'acf-frontend' ),
						'main'			=> __( 'Main', 'acf-frontend' ),
						'article'		=> __( 'Article', 'acf-frontend' ),
						'section'		=> __( 'Section', 'acf-frontend' ),
						'aside'			=> __( 'Aside', 'acf-frontend' ),
					);
					break;
				case 'text':
					$key = __( 'Text Formats', 'acf-frontend' );
					$add_types = array(
						'p'				=> __( 'Paragraph', 'acf-frontend' ),
						'h1'			=> __( 'Heading 1', 'acf-frontend' ),
						'h2'			=> __( 'Heading 2', 'acf-frontend' ),
						'h3'			=> __( 'Heading 3', 'acf-frontend' ),
						'h4'			=> __( 'Heading 4', 'acf-frontend' ),
						'h5'			=> __( 'Heading 5', 'acf-frontend' ),
						'h6'			=> __( 'Heading 6', 'acf-frontend' ),
						'blockquote'	=> __( 'Blockquote', 'acf-frontend' ),
						'pre'			=> __( 'Preformatted', 'acf-frontend' ),
						'address'		=> __( 'Preformatted', 'acf-frontend' ),
					);
					break;
				case 'inline':
					$key = __( 'Inline', 'acf-frontend' );
					$add_types = array(
						'span'		=> __( 'Span', 'acf-frontend' ),
						'strong'	=> __( 'Strong', 'acf-frontend' ),
						'em'		=> __( 'Emphasized', 'acf-frontend' ),
						'big'		=> __( 'Big', 'acf-frontend' ),
						'small'		=> __( 'Small', 'acf-frontend' ),
						'code'		=> __( 'Code', 'acf-frontend' ),
					);
					break;
				// case 'property': // only text + color + integer
				// 	$key = __( 'Property', 'acf-frontend' );
				// 	$add_types = array(
				// 		// color, bg-color, title-attribute, arbitrary attribute
				// 	);
				// 	break;
			}
			$add_types = apply_filters( "acf_frontend_wrapper_types_{$section}", $add_types );
			if ( $key && ! empty($add_types) ) {
				$types[ $key ] = $add_types;
			}
		}
		return $types;
	}


	/**
	 *	@action 'acf/input/admin_head'
	 */
	public function add_meta_box() {
		add_filter( 'postbox_classes_acf-field-group_acf-field-group-frontend', array( $this, 'postbox_classes' ) );
		add_meta_box( 'acf-field-group-frontend', __("Frontend",'acf-frontend'), array( $this, 'locations_metabox'), 'acf-field-group', 'normal', 'high');
	}

	/**
	 *	@filter 'postbox_classes_{$page}-{$id}'
	 */
	public function postbox_classes($classes) {
		$classes[] = 'acf-postbox';
		return $classes;
	}

	/**
	 *	@action 'acf/input/admin_head'
	 */
	public function sort_meta_boxes() {
		global $wp_meta_boxes;

		$new_meta_boxes = array();

		foreach ( $wp_meta_boxes['acf-field-group']['normal']['high'] as $id => $meta_box) {
			if ( $id === 'acf-field-group-frontend' ) {
				continue;
			}

			$new_meta_boxes[$id] = $meta_box;

			if ( $id === 'acf-field-group-locations' ) {
				$new_meta_boxes['acf-field-group-frontend'] = $wp_meta_boxes['acf-field-group']['normal']['high']['acf-field-group-frontend'];
			}
		};
		$new_meta_boxes[$id] = $wp_meta_boxes['acf-field-group']['normal']['high'] = $new_meta_boxes;
	}

	/**
	 *	Metabox callback
	 */
	public function locations_metabox() {
		global $field_group;
		$defaults = array(
			'frontend_active'		=> false,
			'frontend_wrapper'		=> array(
				'type'		=> '',
				'class'		=> '',
				'id'		=> '',
			),
			'frontend_location'		=> array(
				'content'	=> '',
				'widget'	=> '',
			),
		);
		foreach ( $defaults as $key => $default_val ) {
			if ( ! isset( $field_group[ $key ] ) || ! $field_group[ $key ] ) {
				$field_group[ $key ] = $default_val;
			}
		}

		// + frontend wrapper class

		// + frontend wrapper id


		// + frontend location

		acf_get_view( ACF_FRONTEND_DIRECTORY . '/include/views/admin/field-group-frontend.php', array( 'field_group' => $field_group ) );

	}

}

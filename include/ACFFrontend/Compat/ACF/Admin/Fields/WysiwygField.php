<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use ACFFrontend\Compat\ACF\Admin;

class WysiwygField extends SingleScalar {
	/**
	 *	@inheritdoc
	 */
	public function get_wrapper_types( ) {
		$admin = Admin\FieldGroup::instance();
		add_filter( "acf_frontend_wrapper_types_block", array( $this, 'block_wrappers') );
		$types = $admin->get_wrapper_types( 'block' );
		remove_filter( "acf_frontend_wrapper_types_block", array( $this, 'block_wrappers') );
		return $types;
	}

	/**
	 *	@inheritdoc
	 */
	public function block_wrappers( $types ) {
		unset( $types['p'], $types['blockquote'], $types['pre'] );
		return $types;
	}

	/**
	 *	@inheritdoc
	 */
	public function get_attribute_placeholders( ) {
		return array(
			'name'						=> __('Field Name','acf-frontend'),
			'key'						=> __('Field Key','acf-frontend'),
//			'value'						=> __('Field Value','acf-frontend'),
//			'value:<sub_field_name>'	=> __('Sub Field Value','acf-frontend'),
		);

	}

}

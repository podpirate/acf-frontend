<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;
use ACFFrontend\Compat\ACF\Fields as FrontendFields;

class SingleScalar extends Generic {

	/**
	 *	@inheritdoc
	 */
	public function is_supported() {
		return true;
	}


	public function get_wrapper_types( ) {
		$admin = Admin\FieldGroup::instance();
		$types = $admin->get_wrapper_types( 'block', 'inline' );
		return $types;
	}

	public function get_attribute_placeholders( ) {
		return array(
			'id'						=> __('Field ID','acf-frontend'),
			'name'						=> __('Field Name','acf-frontend'),
			'key'						=> __('Field Key','acf-frontend'),
			'value'						=> __('Field Value','acf-frontend'),
//			'value:<sub_field_name>'	=> __('Sub Field Value','acf-frontend'),
		);

	}


}

<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;

class Relation extends Sequence {

	/**
	 *	@inheritdoc
	 */
	public function get_wrapper_types( ) {
		$admin = Admin\FieldGroup::instance();
		$types = $admin->get_wrapper_types( 'sequence' );
		return $types;
	}

	/**
	 *	@inheritdoc
	 */
	public function render_acf_settings( $field ) {
		// wrapper: type, class, id
		$admin = Admin\FieldGroup::instance();

		// $admin->render_frontend_toggle( $field );
		//
		// $admin->render_wrapper_setting( $field + array(
		// 	'wrapper_type_choices'	=> $admin->get_wrapper_types( 'block', 'inline' ),
		// ));
		$admin->render_content_setting( $field );
	}

	/**
	 *	@inheritdoc
	 */
	public function get_attribute_placeholders( ) {
		return array(
			'name'		=> __('Field Name','acf-frontend'),
			'key'		=> __('Field Key','acf-frontend'),
//			'value'		=> __('Field Value','acf-frontend'),
//			'i'			=> __('Counter','acf-frontend'),
		);
	}

}

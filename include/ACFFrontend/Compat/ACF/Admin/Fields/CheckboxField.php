<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;

class CheckboxField extends Sequence {

	/**
	 *	@inheritdoc
	 */
	public function get_wrapper_types( ) {
		add_filter( "acf_frontend_wrapper_types_sequence", array( $this, 'sequence_wrappers') );
		$types = parent::get_wrapper_types();
		remove_filter( "acf_frontend_wrapper_types_sequence", array( $this, 'sequence_wrappers') );
		return $types;
	}

	/**
	 *	@inheritdoc
	 */
	public function sequence_wrappers( $types ) {
		return array_intersect_key($types,array('ul'=>'','ol'=>''));
	}


}

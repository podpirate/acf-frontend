<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;

class Sequence extends SingleScalar {

	/**
	 *	@inheritdoc
	 */
	public function get_wrapper_types( ) {
		$admin = Admin\FieldGroup::instance();
		$types = $admin->get_wrapper_types( 'sequence', 'block', 'inline' );
		return $types;
	}


	/**
	 *	@inheritdoc
	 */
	public function get_attribute_placeholders( ) {
		return array(
			'name'		=> __('Field Name','acf-frontend'),
			'key'		=> __('Field Key','acf-frontend'),
//			'value'		=> __('Field Value','acf-frontend'),
			'i'			=> __('Counter','acf-frontend'),
		);
	}

}

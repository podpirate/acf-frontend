<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;

class Time extends SingleScalar {

	public function get_wrapper_types( ) {
		$admin = Admin\FieldGroup::instance();

		add_filter( "acf_frontend_wrapper_types_inline", array( $this, 'add_time_wrapper') );
		$types = $admin->get_wrapper_types( 'block', 'inline' );
		remove_filter( "acf_frontend_wrapper_types_inline", array( $this, 'add_time_wrapper') );

		return $types;
	}
	public function add_time_wrapper( $types ) {
		$types['time']	= __( 'Time', 'acf-frontend' );
		return $types;
	}

	public function get_attribute_placeholders( ) {
		return array(
			'name'						=> __('Field Name','acf-frontend'),
			'key'						=> __('Field Key','acf-frontend'),
//			'value'						=> __('Field Value','acf-frontend'),
//			'value:<sub_field_name>'	=> __('Sub Field Value','acf-frontend'),
		);

	}

}

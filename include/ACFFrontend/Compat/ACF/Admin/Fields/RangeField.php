<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}
use ACFFrontend\Compat\ACF\Admin;

class RangeField extends SingleScalar {

	/**
	 *	@inheritdoc
	 */
	public function render_acf_settings( $field ) {
		// wrapper: type, class, id
		$admin = Admin\FieldGroup::instance();
		//
		// $admin->render_frontend_toggle( $field );
		//
		// $admin->render_wrapper_setting( $field + array(
		// 	'wrapper_type_choices'	=> $admin->get_wrapper_types( 'block', 'inline' ),
		// ));
		$admin->render_frontend_attribute( $field );
	}

	/**
	 *	@inheritdoc
	 */
	public function get_wrapper_types( ) {
		$admin = Admin\FieldGroup::instance();

		add_filter( "acf_frontend_wrapper_types_inline", array( $this, 'add_progress_wrapper') );
		$types = $admin->get_wrapper_types( 'block', 'inline' );
		remove_filter( "acf_frontend_wrapper_types_inline", array( $this, 'add_progress_wrapper') );

		return $types;
	}

	public function add_progress_wrapper( $types ) {
		$types['progress']	= __( 'Progress', 'acf-frontend' );
		return $types;
	}

}

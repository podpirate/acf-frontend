<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use ACFFrontend\Core;
use ACFFrontend\Compat\ACF\Fields as FrontendFields;

abstract class Field extends FrontendFields\Field {

	/**
	 *	@return bool
	 */
	abstract function is_supported( );

	/**
	 *	@return array
	 */
	abstract function get_wrapper_types( );

	/**
	 *	@return array
	 */
	abstract function get_attribute_placeholders( );


}

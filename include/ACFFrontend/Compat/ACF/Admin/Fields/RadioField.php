<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}
use ACFFrontend\Compat\ACF\Admin;

class RadioField extends SingleScalar {
	/**
	 *	@inheritdoc
	 */
	public function render_acf_settings( $field ) {
		// wrapper: type, class, id
		$admin = Admin\FieldGroup::instance();
		//
		// $admin->render_frontend_toggle( $field );
		//
		// $admin->render_wrapper_setting( $field + array(
		// 	'wrapper_type_choices'	=> $admin->get_wrapper_types( 'block', 'inline' ),
		// ));
		$admin->render_frontend_attribute( $field );
	}

}

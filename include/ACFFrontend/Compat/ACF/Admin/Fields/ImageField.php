<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;

class ImageField extends SingleScalar {

	/**
	 *	@inheritdoc
	 */
	public function render_acf_settings( $field ) {
		// wrapper: type, class, id
//		$admin = Admin\FieldGroup::instance();

		// image size to render
		$field = wp_parse_args( $field, array(
			'frontend_image_size' => true,
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Frontend Image Size','acf-frontend'),
			'instructions'	=> '',
			'type'			=> 'select',
			'name'			=> 'frontend_image_size',
			'class'			=> 'acf-frontend-link',
			'choices'		=> apply_filters('images_size_names_choose', array(
				'thumbnail' => __( 'Thumbnail' ),
				'medium'    => __( 'Medium' ),
				'large'     => __( 'Large' ),
				'full'      => __( 'Full Size' ),
			)),
			'wrapper'		=> array(
				'data-frontend-setting'	=> 'image-size',
				'class'	=> 'acf-field-setting-frontend-image-size',
			),
		));
	}
}

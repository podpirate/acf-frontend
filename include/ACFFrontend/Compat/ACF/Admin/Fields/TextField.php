<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;

class TextField extends SingleScalar {

	/**
	 *	@inheritdoc
	 */
	public function render_acf_settings( $field ) {
		// wrapper: type, class, id
		$admin = Admin\FieldGroup::instance();
		//
		// $admin->render_frontend_toggle( $field );
		//
		// $admin->render_wrapper_setting( $field + array(
		// 	'wrapper_type_choices'	=> $admin->get_wrapper_types( 'block', 'inline' ),
		// ));
		$admin->render_frontend_attribute( $field );
	}

	public function get_wrapper_types( ) {
		$admin = Admin\FieldGroup::instance();
		$types = $admin->get_wrapper_types( 'sequence', 'block', 'text' );
		return $types;
	}
}

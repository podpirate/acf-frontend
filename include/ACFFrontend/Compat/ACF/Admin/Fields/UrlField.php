<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

class UrlField extends Link {
	public function get_attribute_placeholders( ) {
		return array(
			'name'						=> __('Field Name','acf-frontend'),
			'key'						=> __('Field Key','acf-frontend'),
//			'value'						=> __('Field Value','acf-frontend'),
//			'value:<sub_field_name>'	=> __('Sub Field Value','acf-frontend'),
		);

	}

}

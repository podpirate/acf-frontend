<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Fields as FrontendFields;

class Generic extends FrontendFields\Generic {


	/**
	 *	@inheritdoc
	 */
	public function is_supported() {
		return false;
	}

	/**
	 *	@inheritdoc
	 */
	public function render_acf_settings( $field ) {
	}

	/**
	 *	@inheritdoc
	 */
	public function get_wrapper_types( ) {
		return array();
	}

	/**
	 *	@inheritdoc
	 */
	public function get_attribute_placeholders( ) {
		return array();
	}


}

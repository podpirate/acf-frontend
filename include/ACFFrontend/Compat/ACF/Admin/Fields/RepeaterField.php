<?php

namespace ACFFrontend\Compat\ACF\Admin\Fields;

if ( ! defined( 'ABSPATH' ) ) {
	die('Nope.');
}

use ACFFrontend\Compat\ACF\Admin;

class RepeaterField extends Sequence {
	public function get_wrapper_types( ) {
		add_filter( "acf_frontend_wrapper_types_sequence", array( $this, 'sequence_wrappers') );
		$types = parent::get_wrapper_types();
		remove_filter( "acf_frontend_wrapper_types_sequence", array( $this, 'sequence_wrappers') );
		return $types;
	}

	public function sequence_wrappers( $types ) {
		$types['table']	= __( 'Table', 'acf-frontend' );
		return $types;
	}
}

<?php

namespace ACFFrontend\Core;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use ACFFrontend\Compat\ACF\Fields;

class Renderer extends Singleton {




	/**
	 *	@param array		$field_group
	 *	@param int|string	$post_id
	 *	@return string
	 */
	public function render_field_group( $field_group, $post_id ) {
		// get field values

		return $this->render_sequence( $field_group, acf_get_fields($field_group['ID']), $post_id );


	}


	/**
	 *	@param array $field
	 *	@param array $sub_fields
	 *	@param int|string $post_id
	 *	@return string
	 */
	public function render_sequence( $field, $sub_fields, $post_id, $placeholders = array() ) {
		//
		$output = '';

		$field_tag = $label_tag = $row_tag = $rows_tag = '';

		switch ( $field['frontend_wrapper']['type'] ) {
			case '':
				break;
			case 'table':
				$row_tag = 'tr';
				$label_tag = 'th';
				$field_tag = 'td';
				break;
			case 'tr':
				$field_tag = 'td';
				break;
			case 'nav':
				$rows_tag = 'ul';
				$field_tag = 'li';
				break;
			case 'dl':
				$label_tag = 'dt';
				$field_tag = 'dd';
				break;
			case 'ul':
			case 'ol':
				$row_tag = 'li';
				break;
			default:
				break;
		}

		// $output .= $group_before;

		foreach ( $sub_fields as $sub_field ) {
			// $field: single scalar OR acf field

			if ( $field_value = $this->render_field( $sub_field, $post_id ) ) {

				$row = '';
				// add label
				if ( is_array( $sub_field ) && ! empty( $label_tag ) ) {
					$row .= $this->wrap( $sub_field['label'], array( 'type' => $label_tag ) );
				}
				// add field
				$row .= $this->wrap( $field_value, array( 'type' => $field_tag ) );

				// wrap row
				$output .= $this->wrap( $row, array( 'type' => $row_tag ) );
			}

		}
		if ( ! empty( $rows_tag ) ) {
			$this->wrap( $output, array( 'type' => $rows_tag ) );
		}

		// filter attributes
		$wrapper_props = array(
			'type'	=> $field['frontend_wrapper']['type'],
			'attr'	=> apply_filters( "acf_frontend_wrapper_attributes_{$field['ID']}", $field['frontend_wrapper']['attr'], $post_id ),
		);
		$output = $this->wrap( $output, $wrapper_props, $placeholders );
		return $output;
	}




	/**
	 *	@param array|string	$acf_field
	 *	@param int|string	$post_id
	 *	@return string
	 */
	private function render_field( $acf_field, $post_id ) {

		if ( $this->is_acf_field( $acf_field ) ) {

			if ( ! isset( $acf_field['frontend'] ) || ! $acf_field['frontend'] ) {
				return '';
			}

			$field = Fields\Field::getFieldObject( $acf_field );

			return $field->render_output( $post_id, array() );

		} else if ( is_array( $acf_field ) ) {
			$acf_field = implode( ', ', $acf_field );
		}
		return $acf_field;
	}
	/**
	 *	@param array $acf_field
	 *	@return boolean
	 */
	private function is_acf_field( $acf_field ) {
		if ( ! is_array( $acf_field ) ) {
			return false;
		}
		if ( ! isset($acf_field['key']) ) {
			return false;
		}
		return true;
	}



	/**
	 *	@param string	$str
	 *	@param array	$wrapper
	 *	@return string
	 */
	public function wrap( $str, $wrapper = array(), $placeholders = array() ) {

		$wrapper = wp_parse_args( $wrapper, array(
			'type'	=> '',
			'attr'	=> array(
				'class' => '',
				'id'	=> '',
			)
		));

		if ( empty( $wrapper['type'] ) ) {
			return $str;
		}

		return sprintf(
			'<%s %s>%s</%s>',
			$wrapper['type'],
			$this->mk_attr( $wrapper['attr'], $placeholders ),
			$str,
			$wrapper['type']
		);
	}



	/**
	 *	@param array	$attr
	 *	@return string
	 */
	public function mk_attr( $attr = array(), $placeholders = array() ) {
		$output = '';
		foreach ( $attr as $att => $val ) {
			if ( empty( $val ) ) {
				continue;
			}

			foreach ( $placeholders as $tpl => $repl ) {
				$val = str_replace( '{'.$tpl.'}', $repl, $val );
			}

			$att = sanitize_key( $att );
			switch ( $att ) {
				case 'class':
					$val = explode( ' ', $val );
					$val = array_map( 'sanitize_html_class', $val );
					$val = implode( ' ', $val );
					break;
				case 'id':
					$val = sanitize_title( $val );
					break;
				default:
					$val = esc_attr( $val );
					break;
			}
			$output .= sprintf( ' %s="%s"', $att, $val );
		}
		return $output;
	}


}

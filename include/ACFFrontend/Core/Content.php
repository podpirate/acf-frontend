<?php

namespace ACFFrontend\Core;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use ACFFrontend\Compat\ACF\Fields;

class Content extends Singleton {

	/**
	 * @inheritdoc
	 */
	protected function __construct() {

		$this->renderer = Renderer::instance();

		add_filter( 'the_content', array( $this, 'the_content' ) );

	}

	/**
	 *	@filter the_content
	 */
	public function the_content( $content ) {

		if ( ! in_the_loop() ) {
			return $content;
		}

		$post_id = get_the_ID();

		return $this->render( $content, $post_id );
	}
	/**
	 *	@param string $content
	 *	@param int $post_id
	 *	@return string
	 */
	public function render( $content, $post_id ) {



//		isset( $field_group['frontend_active'] ) && $field_group['frontend_active'];

		$field_groups = acf_get_field_groups( array( 'post_id' => $post_id ) );

		$field_groups_to_render = array();
		foreach ( $field_groups as $field_group ) {
			if ( ! isset( $field_group['frontend_active'] ) || ! $field_group['frontend_active'] ) {
				continue;
			}
//			$rendered = $this->render_field_group( $field_group );
			switch ( $field_group['frontend_location']['content'] ) {
				case 'content':
					$field_groups_to_render = array( $field_group );
					break;
				case 'content_after':
				case 'content_before':
					$field_groups_to_render[] = $field_group;
					break;
			}
		}

		foreach ( $field_groups_to_render as $field_group ) {

			$rendered = $this->renderer->render_field_group( $field_group, $post_id );

			switch ( $field_group['frontend_location']['content'] ) {
				case 'content':
					$content = $rendered;
					break;
				case 'content_before':
					$content = $rendered . $content;
					break;
				case 'content_after':
					$content .= $rendered;
					break;
			}
		}

		return $content;
	}

}

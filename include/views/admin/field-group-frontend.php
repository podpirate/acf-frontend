<div class="acf-fields -left">
<?php

$frontend_locations = array(
	''					=> __( 'Off', 'acf-frontend' ),
	'content_after'		=> __( 'After Content', 'acf-frontend' ),
	'content_before'	=> __( 'Before Content', 'acf-frontend' ),
	'content'			=> __( 'Replace Content', 'acf-frontend' ),
	'widget'			=> __( 'Widget Content', 'acf-frontend' ),
	'widget_before'		=> __( 'Before Widget', 'acf-frontend' ),
);

// active
acf_render_field_wrap(array(
	'label'			=> __('Frontend Rendering','acf-frontend'),
	'instructions'	=> '',
	'type'			=> 'true_false',
	'name'			=> 'frontend_active',
	'prefix'		=> 'acf_field_group',
	'value'			=> $field_group['frontend_active'],
	'ui'			=> 1,
	'ui_on_text'	=> __('On', 'acf'),
	'ui_off_text'	=> __('Off', 'acf'),
));



acf_render_field_wrap(array(
	'label'			=> __('Post Content','acf-frontend'),
	'instructions'	=> '',
	'name'			=> 'content',
	'type'			=> 'radio',
	'prefix'		=> 'acf_field_group[frontend_location]',
	'value'			=> $field_group['frontend_location']['content'],
	'ui'			=> 0,
	'layout'		=> 'horizontal',
	'choices'		=> array(
		''					=> __( 'Off', 'acf-frontend' ),
		'content_after'		=> __( 'After Content', 'acf-frontend' ),
		'content_before'	=> __( 'Before Content', 'acf-frontend' ),
		'content'			=> __( 'Replace Content', 'acf-frontend' ),
	),
));

acf_render_field_wrap(array(
	'label'			=> __('Widget','acf-frontend'),
	'instructions'	=> '',
	'name'			=> 'widget',
	'type'			=> 'radio',
	'prefix'		=> 'acf_field_group[frontend_location]',
	'value'			=> $field_group['frontend_location']['widget'],
	'ui'			=> 0,
	'layout'		=> 'horizontal',
	'choices'		=> array(
		''					=> __( 'Off', 'acf-frontend' ),
		'widget'			=> __( 'Replace Widget Content', 'acf-frontend' ),
		'widget_before'		=> __( 'Before Widget', 'acf-frontend' ),
	),
));


// wrapper: type, class, id
acf_render_field_wrap(array(
	'label'					=> __('Wrapper','acf-frontend'),
	'instructions'			=> '',
	'type'					=> 'frontend_wrapper',
	'prefix'				=> 'acf_field_group',
	'frontend_wrapper'		=> $field_group['frontend_wrapper'],
	'frontend_placeholders'	=> array(),
	'ui'					=> 0,
	'wrapper_type_choices'	=> array(
		''	=> __( 'No Wrapper', 'acf-frontend' ),
		__( 'Sequential', 'acf-frontend' )	=> array(
			'table'		=> __( 'Table Rows', 'acf-frontend' ),
			'ul'		=> __( 'Unordered List', 'acf-frontend' ),
			'ol'		=> __( 'Ordered List', 'acf-frontend' ),
			'dl'		=> __( 'Definition List', 'acf-frontend' ),
		),
		__( 'Block Level', 'acf-frontend' )	=> array(
			'p'			=> __('Paragraph','acf-frontend'),
			'div'		=> __('Div','acf-frontend'),
			'article'	=> __('Article','acf-frontend'),
			'section'	=> __('Section','acf-frontend'),
			'aside'		=> __('Aside','acf-frontend'),
		),
	),
));




// Locations	frontend_location
//		[x] wp_foot
//		(x) Content Off  ( ) Replace Content  ( ) Before Content  ( ) After Content
//		(x) Widget Off ( ) Before Widget ( ) After Widget  [Select Widget]

// √ Wrapper		frontend_wrapper_type		select
//		Type : table | dl | div | main | section | article | aside


// Wrapper		frontend_wrapper_labels		true_false ()
// Wrapper		frontend_wrapper_class		text
// Wrapper		frontend_wrapper_id			text


?>
</div>

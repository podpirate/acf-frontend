<?php

namespace ACFFrontend\Core;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use ACFFrontend\Compat;
use ACFFrontend\Fields;
use ACFFrontend\Widgets;

class Core extends Plugin {

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		add_action( 'plugins_loaded' , array( $this , 'plugins_loaded_early' ), 0 );

		add_action( 'plugins_loaded' , array( $this , 'load_textdomain' ) );

		Content::instance();
		Widgets\WidgetFactory::instance();

		parent::__construct();
	}


	/**
	 *	Load frontend styles and scripts
	 *
	 *	@action wp_enqueue_scripts
	 */
	public function wp_enqueue_style() {
	}


	/**
	 *	Load Compatibility classes
	 *
	 *  @action plugins_loaded
	 */
	public function plugins_loaded_early() {
		// early return if no acf
		if ( ! function_exists('acf') || ! class_exists('acf') || version_compare( acf()->version, '5.6', '<' ) ) {
			if ( is_admin() && current_user_can( 'activate_plugins' ) ) {
				add_action( 'admin_notices', array( $this, 'print_no_acf_notice' ) );
			}
			return;
		}

		add_action( 'init' , array( $this , 'init' ) );
		add_action( 'wp_enqueue_scripts' , array( $this , 'wp_enqueue_style' ) );
		// init output Class

		Renderer::instance();

		Compat\ACF\ACF::instance();

	}

	/**
	 * @action admin_notices
	 */
	public function print_no_acf_notice() {
		?>
		<div class="notice notice-error is-dismissible">
			<p><?php
				printf(
					_x( 'The <strong>ACF QuickEdit Fields</strong> plugin requires <a href="%1$s">ACF version 5.6 or later</a>. You can disable and uninstall it on the <a href="%2$s">plugins page</a>.',
						'1: ACF Pro URL, 2: plugins page url',
						'acf-quick-edit-fields'
					),
					'http://www.advancedcustomfields.com/',
					admin_url('plugins.php' )

				);
			?></p>
		</div>
		<?php
	}

	/**
	 *	Load text domain
	 *
	 *  @action plugins_loaded
	 */
	public function load_textdomain() {
		$path = pathinfo( dirname( ACF_FRONTEND_FILE ), PATHINFO_FILENAME );
		load_plugin_textdomain( 'acf-frontend' , false, $path . '/languages' );
	}

	/**
	 *	Init hook.
	 *
	 *  @action init
	 */
	public function init() {
	}

	/**
	 *	Get asset url for this plugin
	 *
	 *	@param	string	$asset	URL part relative to plugin class
	 *	@return wp_enqueue_editor
	 */
	public function get_asset_url( $asset ) {
		return plugins_url( $asset, ACF_FRONTEND_FILE );
	}



}
